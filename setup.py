__author__ = 'lux'

from setuptools import setup

setup(
    name='sugar-api',
    version='0.0.3',
    author='lux',
    author_email='lux@sugarush.io',
    url='https://gitlab.com/sugarush/sugar-api',
    packages=[
        'sugar_api'
    ],
    description='Asynchronous JSON API server components for Quart.',
    install_requires=[
        'sugar-asynctest@git+https://gitlab.com/sugarush/sugar-asynctest@master',
        'sugar-document@git+https://gitlab.com/sugarush/sugar-document@master',
        'sugar-router@git+https://gitlab.com/sugarush/sugar-router@master',
        'sugar-concache@git+https://gitlab.com/sugarush/sugar-concache@master',
        'sugar-odm@git+https://gitlab.com/sugarush/sugar-odm@master',
        'quart',
        'pyjwt',
        'aioredis'
    ]
)
