from copy import copy

from . error import Error

def apply_restrictions(attributes, restrictions=None, token=None, model=None):
    if not restrictions:
        return
    if not token:
        token = { }
    token_data = token.get('data', { })
    token_groups = copy(token_data.get('groups', ['unauthorized']))
    token_id = token_data.get('id', 'unauthorized')
    if model and (model.id == token_id):
        token_groups.append('self')
    def restrict(attributes, restrictions, path=[ ]):
        for (key, allowed_groups) in restrictions.items():
            if isinstance(allowed_groups, dict):
                if attributes.get(key):
                    _path = copy(path)
                    _path.append(key)
                    restrict(attributes[key], restrictions[key], _path)
            else:
                if not _check_restrictions(token_groups, allowed_groups, model, token_id):
                    if attributes.get(key):
                        del attributes[key]
            if isinstance(attributes.get(key), dict):
                if not attributes.get(key):
                    del attributes[key]
    restrict(attributes, restrictions)

def _check_restrictions(token_groups, allowed_groups, model, token_id):
    for group in allowed_groups:
        if group.startswith('$'):
            value = _get_value(group.strip('$'), model)
            if token_id and token_id == value:
                return True
        elif group.startswith('#'):
            value = _get_value(group.strip('#'), model)
            if token_id and token_id in value:
                return True
        elif group in token_groups:
            return True
    return False

def _get_value(path, model):
    for key in path.split('.'):
        model = model[key]
    return model
