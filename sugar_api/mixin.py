import json
import asyncio
from copy import copy
from uuid import uuid4
from datetime import datetime
from logging import getLogger

logger = getLogger('sugar.api')

import jsonpatch
from quart import Blueprint, request, websocket

from sugar_router import Router
from sugar_document import Document
from sugar_concache.redis import RedisDB

from . acl import check_acl
from . preflight import preflight
from . error import Error
from . header import accept, content_type, jsonapi
from . webtoken import webtoken
from . rate import rate, socketrate
from . restrictions import apply_restrictions
from . websocket import authenticate, deauthenticate, status


class TimestampMixin(object):

    '''
    A mixin to manage timestamps.
    '''

    def timestamp(self, value):
        '''
        Manage conversions to and from `datetime` objects.
        '''
        if isinstance(value, str):
            if value.endswith('Z'):
                value = value[:-1] # Remove the Z from the javascript timestamp
            return datetime.fromisoformat(value)
        elif isinstance(value, datetime):
            return value
        else:
            return value


class JSONAPIMixin(object):

    __rate__ = (0, 'none')
    __acl__ = None
    __get__ = None
    __set__ = None

    @classmethod
    def from_jsonapi(cls, data):
        id = data.get('id')
        attributes = data.get('attributes')
        model = cls(attributes)
        if id:
            model.id = id
        return model

    def to_jsonapi(self):
        data = { }
        data['type'] = self._table
        data['id'] = self.id
        data['attributes'] = self.serialize(computed=True)
        del data['attributes'][self._primary]
        return data

    def _render(self, token=None):
        data = self.to_jsonapi()
        apply_restrictions(data['attributes'], self.__get__, token, self)
        return data

    @classmethod
    def resource(cls, *args, events=False, **kargs):
        if not len(args) > 0:
            args = [ cls._table ]

        blueprint = Blueprint(*args, __name__, **kargs)

        url = f'/{cls._table}'

        if events:

            @blueprint.websocket(f'{url}/events')
            async def collection_websocket_route():
                return await cls._collection_events()

        @blueprint.route(url, methods=['OPTIONS', 'GET', 'POST'])
        @webtoken
        @rate(*(cls.__rate__ or [ 0, 'none' ]), namespace=cls._table)
        async def collection_http_route(*args, **kargs):
            return await cls._collection_endpoint(*args, **kargs)

        @blueprint.route(f'{url}/<id>', methods=['OPTIONS', 'GET', 'PATCH', 'DELETE'])
        @webtoken
        @rate(*(cls.__rate__ or [ 0, 'none' ]), namespace=cls._table)
        async def document_http_route(*args, **kargs):
            return await cls._document_endpoint(*args, **kargs)

        return blueprint

    @classmethod
    @preflight(methods=['GET', 'POST'])
    @accept(methods=['POST'])
    @content_type(methods=['GET', 'POST'])
    async def _collection_endpoint(cls, token=None):
        fields = None
        fields_json = request.args.get('fields', None)

        try:
            if fields_json:
                fields = json.loads(fields_json)
        except Exception as e:
            logger.info(str(e), exc_info=True)
            error = Error(
                title = 'Read Error',
                detail = str(e),
                status = 403
            )
            return { 'errors': [ error.serialize() ] }, 403, jsonapi()

        if request.method == 'GET':

            if not check_acl('read_all', cls.__acl__, token):
                error = Error(
                    title = 'Read All Error',
                    detail = 'Action denied.',
                    status = 403
                )
                return { 'errors': [ error.serialize() ] }, 403, jsonapi()


            models = [ ]

            query_json = request.args.get('query', '{ }')

            try:
                query = json.loads(query_json)
            except Exception as e:
                logger.info(str(e), exc_info=True)
                error = Error(
                    title = 'Read Error',
                    detail = str(e),
                    status = 403
                )
                return { 'errors': [ error.serialize() ] }, 403, jsonapi()

            sort = request.args.get('sort')

            if sort:

                try:
                    sort = filter(lambda item: item != '', sort.split(','))
                except Exception as e:
                    logger.info(str(e), exc_info=True)
                    error = Error(
                        title = 'Read Error',
                        detail = str(e),
                        status = 403
                    )
                    return { 'errors': [ error.serialize() ] }, 403, jsonapi()

                def prepare(item):
                    result = [ ]

                    if item.startswith('-'):
                        result.append(-1)
                    else:
                        result.append(1)

                    item = item.strip('-')
                    result.insert(0, item)

                    return result

                sort = list(map(prepare, sort))

            offset = int(request.args.get('page[offset]', 0))
            limit = int(request.args.get('page[limit]', 100))

            if limit > 1000:
                limit = 1000

            try:
                async for model in cls.find(query,
                    sort=sort,
                    skip=offset,
                    limit=limit,
                    projection=fields
                ):
                    models.append(model)
            except Exception as e:
                logger.info(str(e), exc_info=True)
                error = Error(
                    title = 'Read Error',
                    detail = str(e),
                    status = 500
                )
                return { 'errors': [ error.serialize() ] }, 403, jsonapi()

            return {
                'data': list(map(lambda model: model._render(token), models)),
                'meta': {
                    'offset': offset,
                    'limit': limit,
                }
            }, jsonapi()

        elif request.method == 'POST':

            if not check_acl('create', cls.__acl__, token):
                error = Error(
                    title = 'Create Error',
                    detail = 'Action denied.',
                    status = 403
                )
                return { 'errors': [ error.serialize() ] }, 403, jsonapi()

            body = await request.get_json()

            if not isinstance(body, dict):
                error = Error(
                    title = 'Create Error',
                    detail = 'Body is not a JSON object.',
                    status = 403
                )
                return { 'errors': [ error.serialize() ] }, 403, jsonapi()

            data = body.get('data')

            if not data:
                error = Error(
                    title = 'Create Error',
                    detail = 'No data supplied.',
                    status = 403
                )
                return { 'errors': [ error.serialize() ] }, 403, jsonapi()

            if not isinstance(data, dict):
                error = Error(
                    title = 'Create Error',
                    detail = 'Data is not a JSON object.',
                    status = 403
                )
                return { 'errors': [ error.serialize() ] }, 403, jsonapi()

            type = data.get('type')

            if not type:
                error = Error(
                    title = 'Create Error',
                    detail = 'Type is missing.',
                    status = 403
                )
                return { 'errors': [ error.serialize() ] }, 403, jsonapi()

            if not type == cls._table:
                error = Error(
                    title = 'Create Error',
                    detail = 'Provided type does not match resource type.',
                    status = 403
                )
                return { 'errors': [ error.serialize() ] }, 403, jsonapi()

            attributes = data.get('attributes')

            if not attributes:
                error = Error(
                    title = 'Create Error',
                    detail = 'No attributes supplied.',
                    status = 403
                )
                return { 'errors': [ error.serialize() ] }, 403, jsonapi()

            if not isinstance(attributes, dict):
                error = Error(
                    title = 'Create Error',
                    detail = 'Attributes is not a JSON object.',
                    status = 403
                )
                return { 'errors': [ error.serialize() ] }, 403, jsonapi()

            apply_restrictions(attributes, cls.__set__, token)

            if not attributes:
                error = Error(
                    title = 'Create Error',
                    detail = 'No attributes left after restrictions.',
                    status = 403
                )
                return { 'errors': [ error.serialize() ] }, 403, jsonapi()

            model = cls(attributes)

            try:
                await model.save()
            except Exception as e:
                error = Error(
                    title = 'Create Error',
                    detail = str(e),
                    status = 403
                )
                return { 'errors': [ error.serialize() ] }, 403, jsonapi()

            return { 'data': model._render(token) }, jsonapi()

    @classmethod
    @preflight(methods=['GET', 'PATCH', 'DELETE'])
    @accept(methods=['PATCH'])
    @content_type(methods=['GET', 'PATCH', 'DELETE'])
    async def _document_endpoint(cls, id, token=None):
        fields = None
        fields_json = request.args.get('fields', None)

        try:
            if fields_json:
                fields = json.loads(fields_json)
        except Exception as e:
            logger.info(str(e), exc_info=True)
            error = Error(
                title = 'Read Error',
                detail = str(e),
                status = 403
            )
            return { 'errors': [ error.serialize() ] }, 403, jsonapi()
        try:
            model = await cls.find_by_id(id, projection=fields)
        except Exception as e:
            error = Error(
                title = 'ID Error',
                detail = str(e),
                status = 403
            )
            return { 'errors': [ error.serialize() ] }, 403, jsonapi()
        if not model:
            error = Error(
                title = 'ID Error',
                detail = 'ID not found.',
                status = 404
            )
            return {
                'data': None,
                'errors': [ error.serialize() ]
            }, 404, jsonapi()
        if request.method == 'GET':
            if not check_acl('read', cls.__acl__, token, model):
                error = Error(
                    title = 'ACL Error',
                    detail = 'Action denied.',
                    status = 403
                )
                return { 'errors': [ error.serialize() ] }, 403, jsonapi()
            return { 'data': model._render(token) }, jsonapi()
        elif request.method == 'PATCH':
            if not check_acl('update', cls.__acl__, token, model):
                error = Error(
                    title = 'ACL Error',
                    detail = 'Action denied.',
                    status = 403
                )
                return { 'errors': [ error.serialize() ] }, 403, jsonapi()

            body = await request.get_json()

            if not isinstance(body, dict):
                error = Error(
                    title = 'Update Error',
                    detail = 'Body is not a JSON object.',
                    status = 403
                )
                return { 'errors': [ error.serialize() ] }, 403, jsonapi()

            data = body.get('data')

            if not data:
                error = Error(
                    title = 'Update Error',
                    detail = 'No data supplied.',
                    status = 403
                )
                return { 'errors': [ error.serialize() ] }, 403, jsonapi()

            if not isinstance(data, dict):
                error = Error(
                    title = 'Update Error',
                    detail = 'Data is not a JSON object.',
                    status = 403
                )
                return { 'errors': [ error.serialize() ] }, 403, jsonapi()

            type = data.get('type')

            if not type:
                error = Error(
                    title = 'Update Error',
                    detail = 'Type is missing.',
                    status = 403
                )
                return { 'errors': [ error.serialize() ] }, 403, jsonapi()

            if not type == cls._table:
                error = Error(
                    title = 'Update Error',
                    detail = 'Provided type does not match resource type.',
                    status = 403
                )
                return { 'errors': [ error.serialize() ] }, 403, jsonapi()

            attributes = data.get('attributes')

            if not attributes:
                error = Error(
                    title = 'Update Error',
                    detail = 'No attributes supplied.',
                    status = 403
                )
                return { 'errors': [ error.serialize() ] }, 403, jsonapi()

            if not isinstance(attributes, dict):
                error = Error(
                    title = 'Update Error',
                    detail = 'Attributes is not a JSON object.',
                    status = 403
                )
                return { 'errors': [ error.serialize() ] }, 403, jsonapi()

            apply_restrictions(attributes, cls.__set__, token, model)

            if not attributes:
                error = Error(
                    title = 'Update Error',
                    detail = 'No attributes left after restrictions.',
                    status = 403
                )
                return { 'errors': [ error.serialize() ] }, 403, jsonapi()

            try:
                model.update(attributes)
            except Exception as e:
                logger.info(str(e), exc_info=True)
                error = Error(
                    title = 'Update Error',
                    detail = str(e),
                    status = 500
                )
                return { 'errors': [ error.serialize() ] }, 403, jsonapi()

            if not model.id == id:
                error = Error(
                    title = 'Update Error',
                    detail = 'The Model\'s ID does not match the ID in the URL.',
                    status = 403
                )
                return { 'errors': [ error.serialize() ] }, 403, jsonapi()

            try:
                await model.save()
            except Exception as e:
                logger.info(str(e), exc_info=True)
                error = Error(
                    title = 'Update Error',
                    detail = str(e),
                    status = 500
                )
                return { 'errors': [ error.serialize() ] }, 500, jsonapi()
            return { 'data': model._render(token) }, jsonapi()
        elif request.method == 'DELETE':
            if not check_acl('delete', cls.__acl__, token, model):
                error = Error(
                    title = 'ACL Error',
                    detail = 'Action denied.',
                    status = 403
                )
                return { 'errors': [ error.serialize() ] }, 403, jsonapi()
            try:
                await model.delete()
            except Exception as e:
                logger.info(str(e), exc_info=True)
                error = Error(
                    title = 'Delete Error',
                    detail = str(e),
                    status = 500
                )
                return { 'errors': [ error.serialize() ] }, 403, jsonapi()
            return { 'data': { 'id': id } }, jsonapi()

    @classmethod
    async def _collection_events(cls):
        state = type('', (), {})()

        state.redis = await RedisDB.connect()

        state.uuid = str(uuid4())
        state.index = { }
        state.token = None
        state.date = { }
        state.id = ''
        state.groups = [ ]

        async def model_watcher(state, model):
            try:
                async for (action, message) in model.watch():
                    result = {
                        'id': model.id,
                        'action': action
                    }
                    if action == 'update':
                        data = json.loads(message)
                        old = model.serialize()
                        new = model.serialize()
                        jsonpatch.apply_patch(new, data['patch'], in_place=True)
                        apply_restrictions(old, cls.__get__, state.token, model)
                        model.update_direct(new)
                        apply_restrictions(new, cls.__get__, state.token, model)
                        patch = jsonpatch.make_patch(old, new)
                        result.update({
                            'patch': list(patch)
                        })
                    await websocket.send(json.dumps(result))
            except asyncio.CancelledError:
                pass

        await websocket.send(json.dumps({
            'action': 'client-id',
            'id': state.uuid
        }))

        router = Router(methods=[
            'authenticate',
            'deauthenticate',
            'status',
            'subscribe',
            'unsubscribe'
        ])

        @router.authenticate('/')
        @socketrate(*(cls.__rate__ or [ 0, 'none' ]), namespace=cls._table)
        async def _authenticate(state, doc):
            await authenticate(state, doc)

        @router.deauthenticate('/')
        @socketrate(*(cls.__rate__ or [ 0, 'none' ]), namespace=cls._table)
        async def _deauthenticate(state, doc):
            await deauthenticate(state, doc)

        @router.status('/')
        @socketrate(*(cls.__rate__ or [ 0, 'none' ]), namespace=cls._table)
        async def _status(state, doc):
            await status(state, doc)

        @router.subscribe('/<id>')
        @socketrate(*(cls.__rate__ or [ 0, 'none' ]), namespace=cls._table)
        async def _subscribe(state, doc, id):
            model = await cls.find_by_id(id)
            if not model:
                await websocket.send(json.dumps({
                    'action': 'id-not-found',
                    'id': id
                }))
                return
            if not check_acl('subscribe', cls.__acl__, state.token, model):
                await websocket.send(json.dumps({
                    'action': 'acl-denied'
                }))
                return
            state.index[id] = asyncio.create_task(model_watcher(state, model))
            await websocket.send(json.dumps({
                'action': 'subscribed',
                'id': id
            }))

        @router.unsubscribe('/<id>')
        @socketrate(*(cls.__rate__ or [ 0, 'none' ]), namespace=cls._table)
        async def _unsubscribe(state, doc, id):
            model = await cls.find_by_id(id)
            if not model:
                await websocket.send(json.dumps({
                    'action': 'id-not-found',
                    'id': id
                }))
                return
            if not check_acl('subscribe', cls.__acl__, state.token, model):
                await websocket.send(json.dumps({
                    'action': 'acl-denied'
                }))
                return
            if id in state.index.keys():
                state.index[id].cancel()
                await state.index[id]
                del state.index[id]
                await websocket.send(json.dumps({
                    'action': 'unsubscribed',
                    'id': id
                }))

        async def socket_reader(state):
            while True:
                try:
                    data = json.loads(await websocket.receive())
                    doc = Document(data)
                    await router.emit(doc.action, doc.path, state, doc)
                except json.JSONDecodeError as e:
                    logger.info(str(e))
                    continue
                except asyncio.CancelledError as e:
                    logger.info(str(e))
                    break

        await asyncio.gather(socket_reader(state))
