import jwt
import json

from quart import websocket

from . acl import check_acl
from . webtoken import WebToken

async def authenticate(state, doc):
    '''
    Authenticate a websocket connection.
    '''
    try:
        state.token = jwt.decode(doc.token, WebToken.get_secret(),
            algorithms=[WebToken.get_algolithm()],
            options=WebToken.get_options()
        )
        state.data = state.token.get('data', { })
        state.id = state.data.get('id', '')
        state.groups = state.data.get('groups', [ ])
    except jwt.ExpiredSignatureError:
        await websocket.send(json.dumps({
            'action': 'token-expired'
        }))
    except jwt.ImmatureSignatureError:
        await websocket.send(json.dumps({
            'action': 'token-immature'
        }))
    except Exception as e:
        await websocket.send(json.dumps({
            'action': 'token-error'
        }))
    else:
        await websocket.send(json.dumps({
            'action': 'authenticated'
        }))

async def deauthenticate(state, doc):
    '''
    Deauthenticate a websocket connection.
    '''
    ids = list(state.index.keys())
    for id in ids:
        del state.index[id]
    state.token = None
    state.dtate = { }
    state.id = ''
    state.groups = [ ]
    await websocket.send(json.dumps({
        'action': 'deauthenticated'
    }))

async def status(state, doc):
    '''
    Trigger an `authenticated` or `deauthenticated` event.
    '''
    await websocket.send(json.dumps({
        'action': 'authenticated' if state.token else 'deauthenticated'
    }))
