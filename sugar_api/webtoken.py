from functools import wraps
from uuid import uuid4
from abc import ABC, abstractmethod

import jwt
from quart import request, Blueprint

from . error import Error
from . header import content_type, accept, jsonapi
from . preflight import preflight
from . rate import rate

__secret__ = str(uuid4())
__algorithm__ = 'HS256'
__options__ = {
    'verify_exp': True,
    'verify_nbf': True
}

def webtoken(handler):
    '''
    Decode the webtoken, if provided, and inject it into
    the request chain's `\*\*kargs` as `token`.
    '''
    @wraps(handler)
    async def decorator(*args, **kargs):
        authorization = request.headers.get('Authorization')
        if authorization:
            authorization = authorization.split(' ')
            if not len(authorization) == 2:
                error = Error(
                    title = 'Invalid Authorization Header',
                    detail = 'The authorization header is invalid.',
                    status = 403
                )
                return { 'errors': [ error.serialize() ] }, 403, jsonapi()
            if authorization[0].lower() == 'bearer':
                token = authorization[1]
                try:
                    kargs['token'] = jwt.decode(token, __secret__,
                        algorithms=[__algorithm__],
                        options=__options__
                    )
                except jwt.ExpiredSignatureError:
                    error = Error(
                        title = 'Invalid Token Error',
                        detail = 'The token has expired.',
                        status = 403
                    )
                    return { 'errors': [ error.serialize() ] }, 403, jsonapi()
                except jwt.ImmatureSignatureError:
                    error = Error(
                        title = 'Invalid Token Error',
                        detail = 'The token is not yet valid.',
                        status = 403
                    )
                    return { 'errors': [ error.serialize() ] }, 403, jsonapi()
                except Exception as e:
                    error = Error(
                        title = 'Invalid Authorization Header',
                        detail = str(e),
                        status = 403
                    )
                    return { 'errors': [ error.serialize() ] }, 403, jsonapi()
            else:
                error = Error(
                    title = 'Invalid Authorization Header',
                    detail = 'The authorization header is invalid.',
                    status = 403
                )
                return { 'errors': [ error.serialize() ] }, 403, jsonapi()
        return await handler(*args, **kargs)
    return decorator


class WebToken(ABC):

    '''
    A base class to be inherited from when implementing a WebToken
    based authentication method.
    '''

    __rate__ = (0, 'none')

    @classmethod
    @abstractmethod
    async def create(cls, attributes):
        '''
        Implement to handle token creation in response to a PUT request.
        '''
        pass

    @classmethod
    @abstractmethod
    async def refresh(cls, token):
        '''
        Implement to handle token refresh in response to a PATCH request.
        '''
        pass

    @classmethod
    def set_secret(cls, secret):
        '''
        Set the shared WebToken secret.
        '''
        global __secret__
        __secret__ = secret

    @classmethod
    def set_algorithm(cls, algorithm):
        '''
        Set the shared WebToken algorithm.
        '''
        global __algorithm__
        __algorithm__ = algorithm

    @classmethod
    def get_secret(cls):
        '''
        Get the shared WebToken secret.
        '''
        return __secret__

    @classmethod
    def get_algolithm(cls):
        '''
        Get the shared WebToken algorithm.
        '''
        return __algorithm__

    @classmethod
    def get_options(cls):
        '''
        Get the shared WebToken options.
        '''
        return __options__

    @classmethod
    def resource(cls, *args, **kargs):
        '''
        Generate and return a Sanic blueprint containing token create and
        refresh methods.
        '''

        url = kargs.get('url', 'authentication').strip('/')
        url = '/{url}'.format(url=url)

        if 'url' in kargs:
            del kargs['url']

        if not len(args) > 0:
            args = [ url ]

        blueprint = Blueprint(*args, __name__, **kargs)

        @blueprint.route(url, methods=['OPTIONS', 'POST', 'PATCH'])
        @webtoken
        @rate(*(cls.__rate__ or [ 0, 'none' ]))
        async def token_route(*args, **kargs):
            return await cls._token(*args, **kargs)

        return blueprint

    @classmethod
    @preflight(methods=['POST', 'PATCH'])
    @accept(methods=['POST', 'PATCH'])
    @content_type(methods=['POST', 'PATCH'])
    async def _token(cls, token=None):
        if request.method == 'POST':
            body = await request.get_json()

            if not isinstance(body, dict):
                error = Error(
                    title = 'Create Token Error',
                    detail = 'Body is not a JSON object.',
                    status = 403
                )
                return { 'errors': [ error.serialize() ] }, 403, jsonapi()

            data = body.get('data')

            if not data:
                error = Error(
                    title = 'Create Token Error',
                    detail = 'No data supplied.',
                    status = 403
                )
                return { 'errors': [ error.serialize() ] }, 403, jsonapi()

            if not isinstance(data, dict):
                error = Error(
                    title = 'Create Token Error',
                    detail = 'Data is not a JSON object.',
                    status = 403
                )
                return { 'errors': [ error.serialize() ] }, 403, jsonapi()

            attributes = data.get('attributes')

            if not attributes:
                error = Error(
                    title = 'Create Token Error',
                    detail = 'No attributes supplied.',
                    status = 403
                )
                return { 'errors': [ error.serialize() ] }, 403, jsonapi()

            if not isinstance(attributes, dict):
                error = Error(
                    title = 'Create Token Error',
                    detail = 'Attributes is not a JSON object.',
                    status = 403
                )
                return { 'errors': [ error.serialize() ] }, 403, jsonapi()
            try:
                payload = await cls.create(attributes)
            except Exception as e:
                error = Error(
                    title = 'Create Token Error',
                    detail = str(e),
                    status = 403
                )
                return { 'errors': [ error.serialize() ] }, 403, jsonapi()

            encoded_token = jwt.encode(payload, __secret__, algorithm=__algorithm__)

            return {
                'data': {
                    'token': encoded_token.decode('utf-8')
                }
            }, jsonapi()
        elif request.method == 'PATCH':
            if not token:
                error = Error(
                    title = 'Refresh Token Error',
                    detail = 'No token provided.',
                    status = 403
                )
                return { 'errors': [ error.serialize() ] }, 403, jsonapi()
            try:
                payload = await cls.refresh(token)
            except Exception as e:
                error = Error(
                    title = 'Refresh Token Error',
                    detail = str(e),
                    status = 403
                )
                return { 'errors': [ error.serialize() ] }, 403, jsonapi()

            encoded_token = jwt.encode(payload, __secret__, algorithm=__algorithm__)

            return {
                'data': {
                    'token': encoded_token.decode('utf-8')
                }
            }, jsonapi()
