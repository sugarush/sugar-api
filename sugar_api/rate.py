from functools import wraps

from quart import request

from sugar_concache.redis import RedisDB

from . header import jsonapi
from . error import Error


__intervals__ = {
    'none': 0,
    'secondly': 1,
    'minutely': 60,
    'hourly': 3600,
    'daily': 86400,
    'weekly': 604800,
    'monthly': 2419200,
    'yearly': 31449600
}


def rate(limit, interval, namespace=None):
    '''
    Rate limit endpoint to `limit` requests per `interval`. If `nampescpace` is
    provided, it will be used instead of the requests path.

    :param limit: The number of requests to limit to.
    :param interval: The interval over which `limit` is to be applied.
    :param namespace: The namespace, defaults to the requests path.

    .. code-block:: python

        @server.get('/v1/endpoint')
        @rate(1, 'secondly')
        async def handler(request):
            ...
    '''
    if not interval in __intervals__:
        raise Exception(
            f'ratelimit: {interval} not in {list(__intervals__.keys())}'
        )
    def wrapper(handler):
        @wraps(handler)
        async def decorator(*args, **kargs):
            if interval is 'none':
                return await handler(*args, **kargs)

            token = kargs.get('token')

            data = (token or { }).get('data', { })
            id = data.get('id')

            redis = await RedisDB.connect()

            key = f'{id or request.host}:{namespace or request.path}'

            count = await redis.get(key) or 0

            if int(count) >= limit:
                error = Error(
                    title = 'Rate Limit Error',
                    detail = f'Rate limit exceeded: {limit} {interval}',
                    status = 403
                )
                return { 'errors': [ error.serialize() ] }, 403, jsonapi()

            if await redis.exists(key):
                await redis.incr(key)
            else:
                await redis.set(key, 1, expire=__intervals__[interval])

            return await handler(*args, **kargs)
        return decorator
    return wrapper

def socketrate(limit, interval, namespace=None):
    '''
    Rate limit endpoint to `limit` requests per `interval`. If `nampescpace` is
    provided, it will be used instead of the requests path.

    :param limit: The number of requests to limit to.
    :param interval: The interval over which `limit` is to be applied.
    :param namespace: The namespace, defaults to the requests path.
    '''
    if not interval in __intervals__:
        raise Exception(
            f'ratelimit: {interval} not in {list(__intervals__.keys())}'
        )
    def wrapper(handler):
        @wraps(handler)
        async def decorator(state, doc, *args, **kargs):
            if interval is 'none':
                return await handler(state, doc, *args, **kargs)

            data = (state.token or { }).get('data', { })
            id = data.get('id')

            redis = await RedisDB.connect()

            key = f'{id or websocket.host}'

            if namespace:
                key += f'{namespace}'
            else:
                key += f'{websocket.path}:{doc.path}'

            count = await redis.get(key) or 0

            if int(count) >= limit:
                await state.socket.send(json.dumps({
                    'action': 'rate-limit',
                    'interval': interval,
                    'limit': limit
                }))
                return None

            if await redis.exists(key):
                await redis.incr(key)
            else:
                await redis.set(key, 1, expire=__intervals__[interval])

            return await handler(state, doc, *args, **kargs)
        return decorator
    return wrapper
