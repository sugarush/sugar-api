from functools import partial, wraps

from quart import request

from . cors import CORS
from . error import Error

def headers(dictionary=None, content_type=None):
    if not dictionary:
        dictionary = { }
    dictionary.update({
        'Access-Control-Allow-Origin': CORS.get_origins(),
    })
    if content_type:
        dictionary.update({
            'Content-Type': content_type
        })
    return dictionary

jsonapi = partial(headers, content_type='application/vnd.api+json')

def accept(content_type='application/vnd.api+json', methods=[ ]):
    def wrapper(handler):
        @wraps(handler)
        async def decorator(*args, **kargs):
            if request.method in methods:
                accept_header = request.headers.get('Accept')
                if not accept_header == content_type:
                    error = Error(
                        title = 'Invalid Accept Header',
                        detail = 'Accept header of invalid type.',
                        status = 403
                    )
                    return { 'errors': [ error.serialize() ] }, 403, jsonapi()
            return await handler(*args, **kargs)
        return decorator
    return wrapper

def content_type(content_type='application/vnd.api+json', methods=[ ]):
    def wrapper(handler):
        @wraps(handler)
        async def decorator(*args, **kargs):
            if request.method in methods:
                content_type_header = request.headers.get('Content-Type')
                if not content_type_header == content_type:
                    error = Error(
                        title = 'Invalid Content-Type Header',
                        detail = 'Content-Type header of invalid type.',
                        status = 403
                    )
                    return { 'errors': [ error.serialize() ] }, 403, jsonapi()
            return await handler(*args, **kargs)
        return decorator
    return wrapper
