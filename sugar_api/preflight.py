from functools import wraps

from quart import request

from . cors import CORS

def preflight(**kwargs):
    def wrapper(handler):
        @wraps(handler)
        async def decorator(*args, **kargs):
            if request.method == 'OPTIONS':
                allowed_methods = kwargs.get('methods', [ ])
                allowed_headers = kwargs.get('headers', ['Accept', 'Authorization', 'Content-Type'])
                headers = {
                    'Access-Control-Allow-Origin': CORS.get_origins(),
                    'Access-Control-Allow-Methods': ', '.join(allowed_methods),
                    'Access-Control-Allow-Headers': ', '.join(allowed_headers)
                }
                return '', headers
            return await handler(*args, **kargs)
        return decorator
    return wrapper
