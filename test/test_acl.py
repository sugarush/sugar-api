from unittest import TestCase
from sugar_document import Document as Doc

from sugar_api.acl import check_acl, _check_action

class ACLTest(TestCase):

    def test_check_action_invalid(self):
        result = _check_action('action', [ ])
        self.assertFalse(result)

    def test_check_action_valid(self):
        result = _check_action('action', [ 'action' ])
        self.assertTrue(result)

    def test_check_action_all(self):
        result = _check_action('action', [ 'all' ])
        self.assertTrue(result)

    def test_check_acl_no_token_invalid(self):
        result = check_acl('action', {
            'unauthorized': [ ]
        })
        self.assertFalse(result)

    def test_check_acl_no_token_valid(self):
        result = check_acl('action', {
            'unauthorized': ['action']
        })
        self.assertTrue(result)

    def test_check_acl_self_invalid(self):

        result = check_acl('action', {
            'self': [ ]
        }, {
            'data': { 'id': 'aabbcc' }
        }, Doc({ 'id': 'aabbcc' }))
        self.assertFalse(result)

    def test_check_acl_self_valid(self):
        result = check_acl('action', {
            'self': ['action']
        }, {
            'data': { 'id': 'aabbcc' }
        }, Doc({ 'id': 'aabbcc' }))
        self.assertTrue(result)

    def test_check_acl_group_invalid(self):
        result = check_acl('action', {
            'test_group': [ ]
        }, {
            'data': { 'groups': [ 'test_group' ] }
        })
        self.assertFalse(result)

    def test_check_acl_group_valid(self):
        result = check_acl('action', {
            'test_group': ['action']
        }, {
            'data': { 'groups': [ 'test_group' ] }
        })
        self.assertTrue(result)

    def test_check_acl_other_invalid(self):
        result = check_acl('action', {
            'other': [ ]
        }, { })
        self.assertFalse(result)

    def test_check_acl_other_valid(self):
        result = check_acl('action', {
            'other': ['action']
        }, {
            'data': { }
        })
        self.assertTrue(result)

    def test_check_acl_other_skip_self(self):
        result = check_acl('action', {
            'self': [ ],
            'other': ['action']
        }, {
            'data': { 'id': 'aabbcc' }
        }, Doc({ 'id': 'aabbcc' }))
        self.assertFalse(result)

    def test_check_acl_other_skip_group(self):
        result = check_acl('action', {
            'test_group': [ ],
            'other': ['action']
        }, {
            'data': { 'groups': [ 'test_group' ] }
        })
        self.assertFalse(result)

    def test_check_acl_self_and_group(self):
        result = check_acl('action', {
            'self': ['action'],
            'test_group': [ ]
        }, {
            'data': { 'id': 'aabbcc' }
        }, Doc({ 'id': 'aabbcc' }))
        self.assertTrue(result)
        result = check_acl('action', {
            'self': [ ],
            'test_group': ['action']
        }, {
            'data': { 'groups': [ 'test_group' ] }
        })
        self.assertTrue(result)

    def test_check_acl_specific_invalid(self):
        result = check_acl('action', {
            '$owner': ['action']
        }, {
            'data': { 'id': 'aabbcc' }
        }, Doc({ 'id': 'aabbcc' }))
        self.assertFalse(result)

    def test_check_acl_specific_valid(self):
        result = check_acl('action', {
            '$owner': ['action']
        }, {
            'data': { 'id': 'aabbcc' }
        }, Doc({ 'owner': 'aabbcc' }))
        self.assertTrue(result)

    def test_check_acl_specific_valid_nested(self):
        result = check_acl('action', {
            '$gamma.owner': ['action']
        }, {
            'data': { 'id': 'aabbcc' }
        }, Doc({ 'gamma': { 'owner': 'aabbcc' } }))
        self.assertTrue(result)

    def test_check_acl_specific_self_skip(self):
        result = check_acl('action', {
            'self': [ ],
            '$owner': ['action']
        }, {
            'data': { 'id': 'aabbcc' }
        }, Doc({ 'id': 'aabbcc', 'owner': 'aabbcc' }))
        self.assertFalse(result)

    def test_check_acl_grouped_invalid(self):
        result = check_acl('action', {
            '#owners': ['action']
        }, {
            'data': { 'id': 'aabbcc' }
        }, Doc({ 'owners': [ 'ddeeff' ] }))
        self.assertFalse(result)

    def test_check_acl_grouped_valid(self):
        result = check_acl('action', {
            '#owners': ['action']
        }, {
            'data': { 'id': 'aabbcc' }
        }, Doc({ 'owners': [ 'aabbcc' ] }))
        self.assertTrue(result)

    def test_check_acl_grouped_valid_nested(self):
        result = check_acl('action', {
            '#gamma.owners': ['action']
        }, {
            'data': { 'id': 'aabbcc' }
        }, Doc({ 'gamma': { 'owners': [ 'aabbcc' ] } }))
        self.assertTrue(result)

    def test_check_acl_grouped_self_skip(self):
        result = check_acl('action', {
            'self': [ ],
            '#owners': ['action']
        }, {
            'data': { 'id': 'aabbcc' }
        }, Doc({ 'id': 'aabbcc', 'owners': [ 'aabbcc' ] }))
        self.assertFalse(result)
