import asyncio
import json
from functools import partial
from unittest import skip

from sugar_asynctest import AsyncTestCase
from sugar_odm import MongoDBModel, Field
from sugar_api import JSONAPIMixin, WebToken
from sugar_concache.redis import RedisDB

from quart import Quart


class Authentication(WebToken):

    @classmethod
    async def create(cls, attributes):
        return attributes

    @classmethod
    async def refresh(cls, token):
        return token


class Mixin(MongoDBModel, JSONAPIMixin):
    field = Field()


class Realtime(MongoDBModel, JSONAPIMixin):
    field = Field()

    __acl__ = {
        'self': ['subscribe']
    }


class RateLimited(MongoDBModel, JSONAPIMixin):
    field = Field()

    __rate__ = (1, 'secondly')


class JSONAPIMixinTest(AsyncTestCase):

    default_loop = True

    async def asyncSetUp(self):
        self.app = Quart('test')
        self.app.register_blueprint(Mixin.resource())
        self.app.register_blueprint(Realtime.resource(events=True))
        self.app.register_blueprint(RateLimited.resource())
        self.app.register_blueprint(Authentication.resource())
        self.client = self.app.test_client()
        self.headers = {
            'Accept': 'application/vnd.api+json',
            'Content-Type': 'application/vnd.api+json'
        }
        await RedisDB.set_event_loop(asyncio.get_event_loop())

    async def asyncTearDown(self):
        await Mixin.drop()
        await Realtime.drop()
        await RateLimited.drop()

    async def test_check_preflight(self):
        response = await self.client.options('/mixins')
        self.assertIn('Access-Control-Allow-Origin', response.headers)

    async def test_check_accept(self):
        response = await self.client.post('/mixins', headers={ 'Accept': 'application/vnd.api+json' })
        data = await response.get_json()
        self.assertEqual(data['errors'][0]['title'], 'Invalid Content-Type Header')

    async def test_check_content_type(self):
        response = await self.client.post('/mixins', headers={ 'Content-Type': 'application/vnd.api+json' })
        data = await response.get_json()
        self.assertEqual(data['errors'][0]['title'], 'Invalid Accept Header')

    async def test_rate(self):
        request = partial(self.client.post, '/rate_limiteds', headers=self.headers)
        await request()
        await asyncio.sleep(0.1)
        response = await request()
        data = await response.get_json()
        self.assertEqual(data['errors'][0]['title'], 'Rate Limit Error')

    async def test_collection_create_body_not_an_object(self):
        response = await self.client.post('/mixins', headers=self.headers, data=json.dumps([ ]))
        data = await response.get_json()
        self.assertEqual(data['errors'][0]['title'], 'Create Error')

    async def test_collection_create_body_empty(self):
        response = await self.client.post('/mixins', headers=self.headers, data=json.dumps({ }))
        data = await response.get_json()
        self.assertEqual(data['errors'][0]['title'], 'Create Error')

    async def test_collection_create_no_type(self):
        response = await self.client.post('/mixins', headers=self.headers, data=json.dumps({
            'data': {
                'attributes': { }
            }
        }))
        data = await response.get_json()
        self.assertEqual(data['errors'][0]['title'], 'Create Error')

    async def test_collection_create_invalid_type(self):
        response = await self.client.post('/mixins', headers=self.headers, data=json.dumps({
            'data': {
                'type': 'invalid',
                'attributes': { }
            }
        }))
        data = await response.get_json()
        self.assertEqual(data['errors'][0]['title'], 'Create Error')

    async def test_collection_create_attributes_empty(self):
        response = await self.client.post('/mixins', headers=self.headers, data=json.dumps({
            'data': {
                'type': 'mixins',
                'attributes': { }
            }
        }))
        data = await response.get_json()
        self.assertEqual(data['errors'][0]['title'], 'Create Error')

    async def test_collection_create_attributes_not_an_object(self):
        response = await self.client.post('/mixins', headers=self.headers, data=json.dumps({
            'data': {
                'type': 'mixins',
                'attributes': [ ]
            }
        }))
        data = await response.get_json()
        self.assertEqual(data['errors'][0]['title'], 'Create Error')

    async def test_collection_create(self):
        response = await self.client.post('/mixins', headers=self.headers, data=json.dumps({
            'data': {
                'type': 'mixins',
                'attributes': {
                    'field': 'value'
                }
            }
        }))
        data = await response.get_json()
        self.assertIsNotNone(data['data']['id'])

    async def test_collection_read(self):
        response = await self.client.post('/mixins', headers=self.headers, data=json.dumps({
            'data': {
                'type': 'mixins',
                'attributes': {
                    'field': 'value'
                }
            }
        }))
        data = await response.get_json()
        response = await self.client.get('/mixins', headers=self.headers)
        data = await response.get_json()
        self.assertEqual(len(data['data']), 1)

    async def test_collection_read_query(self):
        response = await self.client.post('/mixins', headers=self.headers, data=json.dumps({
            'data': {
                'type': 'mixins',
                'attributes': {
                    'field': 'value'
                }
            }
        }))
        data = await response.get_json()
        response = await self.client.get('/mixins?query={ "field": "123" }', headers=self.headers)
        data = await response.get_json()
        self.assertEqual(len(data['data']), 0)

    async def test_collection_read_sort(self):
        await self.client.post('/mixins', headers=self.headers, data=json.dumps({
            'data': {
                'type': 'mixins',
                'attributes': {
                    'field': '1'
                }
            }
        }))
        await self.client.post('/mixins', headers=self.headers, data=json.dumps({
            'data': {
                'type': 'mixins',
                'attributes': {
                    'field': '2'
                }
            }
        }))
        response = await self.client.get('/mixins?sort=-field', headers=self.headers)
        data = await response.get_json()
        self.assertEqual(data['data'][0]['attributes']['field'], '2')

    async def test_collection_read_limit(self):
        await self.client.post('/mixins', headers=self.headers, data=json.dumps({
            'data': {
                'type': 'mixins',
                'attributes': {
                    'field': '1'
                }
            }
        }))
        await self.client.post('/mixins', headers=self.headers, data=json.dumps({
            'data': {
                'type': 'mixins',
                'attributes': {
                    'field': '2'
                }
            }
        }))
        response = await self.client.get('/mixins?page[limit]=1', headers=self.headers)
        data = await response.get_json()
        self.assertEqual(len(data['data']), 1)

    async def test_collection_read_offset(self):
        await self.client.post('/mixins', headers=self.headers, data=json.dumps({
            'data': {
                'type': 'mixins',
                'attributes': {
                    'field': '1'
                }
            }
        }))
        await self.client.post('/mixins', headers=self.headers, data=json.dumps({
            'data': {
                'type': 'mixins',
                'attributes': {
                    'field': '2'
                }
            }
        }))
        response = await self.client.get('/mixins?page[limit]=1&page[offset]=1', headers=self.headers)
        data = await response.get_json()
        self.assertEqual(len(data['data']), 1)

    async def test_document_read(self):
        response = await self.client.post('/mixins', headers=self.headers, data=json.dumps({
            'data': {
                'type': 'mixins',
                'attributes': {
                    'field': 'value'
                }
            }
        }))
        data = await response.get_json()
        response = await self.client.get(f'/mixins/{data["data"]["id"]}', headers=self.headers)
        data = await response.get_json()
        self.assertIsNotNone(data['data']['id'])

    async def test_document_update_body_not_an_object(self):
        response = await self.client.post('/mixins', headers=self.headers, data=json.dumps({
            'data': {
                'type': 'mixins',
                'attributes': {
                    'field': 'value'
                }
            }
        }))
        data = await response.get_json()
        response = await self.client.patch(f'/mixins/{data["data"]["id"]}', headers=self.headers, data=json.dumps([ ]))
        data = await response.get_json()
        self.assertEqual(data['errors'][0]['title'], 'Update Error')

    async def test_document_update_body_empty(self):
        response = await self.client.post('/mixins', headers=self.headers, data=json.dumps({
            'data': {
                'type': 'mixins',
                'attributes': {
                    'field': 'value'
                }
            }
        }))
        data = await response.get_json()
        response = await self.client.patch(f'/mixins/{data["data"]["id"]}', headers=self.headers, data=json.dumps({ }))
        data = await response.get_json()
        self.assertEqual(data['errors'][0]['title'], 'Update Error')

    async def test_document_update_data_empty(self):
        response = await self.client.post('/mixins', headers=self.headers, data=json.dumps({
            'data': {
                'type': 'mixins',
                'attributes': {
                    'field': 'value'
                }
            }
        }))
        data = await response.get_json()
        response = await self.client.patch(f'/mixins/{data["data"]["id"]}', headers=self.headers, data=json.dumps({
            'data': { }
        }))
        data = await response.get_json()
        self.assertEqual(data['errors'][0]['title'], 'Update Error')

    async def test_document_update_data_not_an_object(self):
        response = await self.client.post('/mixins', headers=self.headers, data=json.dumps({
            'data': {
                'type': 'mixins',
                'attributes': {
                    'field': 'value'
                }
            }
        }))
        data = await response.get_json()
        response = await self.client.patch(f'/mixins/{data["data"]["id"]}', headers=self.headers, data=json.dumps({
            'data': [ ]
        }))
        data = await response.get_json()
        self.assertEqual(data['errors'][0]['title'], 'Update Error')

    async def test_document_update_type_invalid(self):
        response = await self.client.post('/mixins', headers=self.headers, data=json.dumps({
            'data': {
                'type': 'mixins',
                'attributes': {
                    'field': 'value'
                }
            }
        }))
        data = await response.get_json()
        response = await self.client.patch(f'/mixins/{data["data"]["id"]}', headers=self.headers, data=json.dumps({
            'data': {
                'type': 'invalid',
            }
        }))
        data = await response.get_json()
        self.assertEqual(data['errors'][0]['title'], 'Update Error')

    async def test_document_update_attributes_empty(self):
        response = await self.client.post('/mixins', headers=self.headers, data=json.dumps({
            'data': {
                'type': 'mixins',
                'attributes': {
                    'field': 'value'
                }
            }
        }))
        data = await response.get_json()
        response = await self.client.patch(f'/mixins/{data["data"]["id"]}', headers=self.headers, data=json.dumps({
            'data': {
                'type': 'mixins',
                'attributes': { }
            }
        }))
        data = await response.get_json()
        self.assertEqual(data['errors'][0]['title'], 'Update Error')

    async def test_document_update_attributes_not_an_object(self):
        response = await self.client.post('/mixins', headers=self.headers, data=json.dumps({
            'data': {
                'type': 'mixins',
                'attributes': {
                    'field': 'value'
                }
            }
        }))
        data = await response.get_json()
        response = await self.client.patch(f'/mixins/{data["data"]["id"]}', headers=self.headers, data=json.dumps({
            'data': {
                'type': 'mixins',
                'attributes': [ ]
            }
        }))
        data = await response.get_json()
        self.assertEqual(data['errors'][0]['title'], 'Update Error')

    async def test_document_update_attributes_not_an_object(self):
        response = await self.client.post('/mixins', headers=self.headers, data=json.dumps({
            'data': {
                'type': 'mixins',
                'attributes': {
                    'field': 'value'
                }
            }
        }))
        data = await response.get_json()
        response = await self.client.patch(f'/mixins/{data["data"]["id"]}', headers=self.headers, data=json.dumps({
            'data': {
                'type': 'mixins',
                'attributes': [ ]
            }
        }))
        data = await response.get_json()
        self.assertEqual(data['errors'][0]['title'], 'Update Error')

    async def test_document_update_attributes_not_an_object(self):
        response = await self.client.post('/mixins', headers=self.headers, data=json.dumps({
            'data': {
                'type': 'mixins',
                'attributes': {
                    'field': 'value'
                }
            }
        }))
        data = await response.get_json()
        response = await self.client.patch(f'/mixins/{data["data"]["id"]}', headers=self.headers, data=json.dumps({
            'data': {
                'type': 'mixins',
                'attributes': {
                    '_id': 'invalid',
                    'field': 'value'
                }
            }
        }))
        data = await response.get_json()
        self.assertEqual(data['errors'][0]['title'], 'Update Error')

    async def test_document_update(self):
        response = await self.client.post('/mixins', headers=self.headers, data=json.dumps({
            'data': {
                'type': 'mixins',
                'attributes': {
                    'field': 'value'
                }
            }
        }))
        data = await response.get_json()
        response = await self.client.patch(f'/mixins/{data["data"]["id"]}', headers=self.headers, data=json.dumps({
            'data': {
                'id': data['data']['id'],
                'type': 'mixins',
                'attributes': {
                    'field': 'value'
                }
            }
        }))
        data = await response.get_json()
        self.assertIsNotNone(data['data']['id'])

    async def test_document_delete(self):
        response = await self.client.post('/mixins', headers=self.headers, data=json.dumps({
            'data': {
                'type': 'mixins',
                'attributes': {
                    'field': 'value'
                }
            }
        }))
        data = await response.get_json()
        response = await self.client.delete(f'/mixins/{data["data"]["id"]}', headers=self.headers)
        data = await response.get_json()
        self.assertIsNotNone(data['data']['id'])

    async def test_events_client_id(self):
        async with self.client.websocket('/realtimes/events') as socket:
            data = json.loads(await socket.receive())
            self.assertIsNotNone(data['id'])

    async def test_events_authenticate(self):
        response = await self.client.post('/authentication', headers=self.headers, data=json.dumps({
            'data': {
                'attributes': {
                    'field': 'value'
                }
            }
        }))
        data = await response.get_json()
        async with self.client.websocket('/realtimes/events') as socket:
            json.loads(await socket.receive())
            await socket.send(json.dumps({
                'action': 'authenticate',
                'path': '/',
                'token': data['data']['token']
            }))
            data = json.loads(await socket.receive())
            self.assertEqual(data['action'], 'authenticated')

    async def test_events_deauthenticate(self):
        response = await self.client.post('/authentication', headers=self.headers, data=json.dumps({
            'data': {
                'attributes': {
                    'field': 'value'
                }
            }
        }))
        data = await response.get_json()
        async with self.client.websocket('/realtimes/events') as socket:
            json.loads(await socket.receive())
            await socket.send(json.dumps({
                'action': 'authenticate',
                'path': '/',
                'token': data['data']['token']
            }))
            data = json.loads(await socket.receive())
            self.assertEqual(data['action'], 'authenticated')
            await socket.send(json.dumps({
                'action': 'deauthenticate',
                'path': '/'
            }))
            data = json.loads(await socket.receive())
            self.assertEqual(data['action'], 'deauthenticated')

    async def test_events_status(self):
        response = await self.client.post('/authentication', headers=self.headers, data=json.dumps({
            'data': {
                'attributes': {
                    'field': 'value'
                }
            }
        }))
        data = await response.get_json()
        async with self.client.websocket('/realtimes/events') as socket:
            json.loads(await socket.receive())
            await socket.send(json.dumps({
                'action': 'status',
                'path': '/'
            }))
            _data = json.loads(await socket.receive())
            self.assertEqual(_data['action'], 'deauthenticated')
            await socket.send(json.dumps({
                'action': 'authenticate',
                'path': '/',
                'token': data['data']['token']
            }))
            data = json.loads(await socket.receive())
            self.assertEqual(data['action'], 'authenticated')
            await socket.send(json.dumps({
                'action': 'status',
                'path': '/'
            }))
            data = json.loads(await socket.receive())
            self.assertEqual(data['action'], 'authenticated')

    async def test_events_subscribe_unauthorized(self):
        model = await Realtime.add({ 'field': 'value' })
        async with self.client.websocket('/realtimes/events') as socket:
            json.loads(await socket.receive())
            await socket.send(json.dumps({
                'action': 'subscribe',
                'path': f'/{model.id}'
            }))
            data = json.loads(await socket.receive())
            self.assertEqual(data['action'], 'acl-denied')

    async def test_events_subscribe_authorized(self):
        model = await Realtime.add({ 'field': 'value' })
        response = await self.client.post('/authentication', headers=self.headers, data=json.dumps({
            'data': {
                'attributes': {
                    'data': {
                        'id': model.id
                    }
                }
            }
        }))
        data = await response.get_json()
        async with self.client.websocket('/realtimes/events') as socket:
            json.loads(await socket.receive())
            await socket.send(json.dumps({
                'action': 'authenticate',
                'path': '/',
                'token': data['data']['token']
            }))
            data = json.loads(await socket.receive())
            self.assertEqual(data['action'], 'authenticated')
            await socket.send(json.dumps({
                'action': 'subscribe',
                'path': f'/{model.id}'
            }))
            data = json.loads(await socket.receive())
            self.assertEqual(data['action'], 'subscribed')
            model.field = '123'
            await model.save()
            data = json.loads(await socket.receive())
            self.assertEqual(data['action'], 'update')

    async def test_events_unsubscribe_unauthorized(self):
        model = await Realtime.add({ 'field': 'value' })
        async with self.client.websocket('/realtimes/events') as socket:
            json.loads(await socket.receive())
            await socket.send(json.dumps({
                'action': 'unsubscribe',
                'path': f'/{model.id}'
            }))
            data = json.loads(await socket.receive())
            self.assertEqual(data['action'], 'acl-denied')

    async def test_events_unsubscribe_authorized(self):
        model = await Realtime.add({ 'field': 'value' })
        response = await self.client.post('/authentication', headers=self.headers, data=json.dumps({
            'data': {
                'attributes': {
                    'data': {
                        'id': model.id
                    }
                }
            }
        }))
        data = await response.get_json()
        async with self.client.websocket('/realtimes/events') as socket:
            json.loads(await socket.receive())
            await socket.send(json.dumps({
                'action': 'authenticate',
                'path': '/',
                'token': data['data']['token']
            }))
            data = json.loads(await socket.receive())
            self.assertEqual(data['action'], 'authenticated')
            await socket.send(json.dumps({
                'action': 'subscribe',
                'path': f'/{model.id}'
            }))
            data = json.loads(await socket.receive())
            self.assertEqual(data['action'], 'subscribed')
            model.field = '123'
            await model.save()
            data = json.loads(await socket.receive())
            self.assertEqual(data['action'], 'update')
            await socket.send(json.dumps({
                'action': 'unsubscribe',
                'path': f'/{model.id}'
            }))
            data = json.loads(await socket.receive())
            self.assertEqual(data['action'], 'unsubscribed')
