from unittest import TestCase, skip

from sugar_document import Document as Doc
from sugar_api.restrictions import apply_restrictions, _get_value


class RestrictionsTest(TestCase):

    def test_get_value(self):

        value = _get_value('test.ing', {
            'test': {
                'ing': '123'
            }
        })

        self.assertEqual(value, '123')

    def test_restrictions_unauthorized_invalid(self):

        attributes = {
            'test': 'ing'
        }

        restrictions = {
            'test': [ ]
        }

        token = {
            'data': {
                'groups': [ ]
            }
        }

        apply_restrictions(attributes, restrictions, token)

        self.assertIsNone(attributes.get('test'))

    def test_restrictions_unauthorized_valid(self):

        attributes = {
            'test': 'ing'
        }

        restrictions = {
            'test': ['unauthorized']
        }

        token = {
            'data': {
                'groups': ['unauthorized']
            }
        }

        apply_restrictions(attributes, restrictions, token)

        self.assertIsNotNone(attributes.get('test'))

    def test_restrictions_allowed(self):

        attributes = {
            'test': 'ing'
        }

        restrictions = { }

        token = {
            'data': {
                'groups': [ ]
            }
        }

        apply_restrictions(attributes, restrictions, token)

        self.assertIsNotNone(attributes.get('test'))

    def test_restrictions_removed(self):

        attributes = {
            'test': 'ing'
        }

        restrictions = {
            'test': ['group']
        }

        token = {
            'data': {
                'groups': [ ]
            }
        }

        apply_restrictions(attributes, restrictions, token)

        self.assertIsNone(attributes.get('test'))

    def test_restrictions_group_allowed(self):

        attributes = {
            'test': 'ing'
        }

        restrictions = {
            'test': ['group']
        }

        token = {
            'data': {
                'groups': ['group']
            }
        }

        apply_restrictions(attributes, restrictions, token)

        self.assertIsNotNone(attributes.get('test'))

    def test_restrictions_nested_allowed(self):

        attributes = {
            'test': {
                'ing': 'value'
            }
        }

        restrictions = {
            'test': {
                'ing': ['group']
            }
        }

        token = {
            'data': {
                'groups': ['group']
            }
        }

        apply_restrictions(attributes, restrictions, token)

        self.assertIsNotNone(attributes['test'].get('ing'))

    def test_restrictions_nested_removed(self):

        attributes = {
            'test': {
                'ing': 'value'
            }
        }

        restrictions = {
            'test': {
                'ing': ['group']
            }
        }

        token = {
            'data': {
                'groups': [ ]
            }
        }

        apply_restrictions(attributes, restrictions, token)

        self.assertIsNone(attributes.get('test'))

    def test_restrictions_specific_allowed(self):

        attributes = {
            'test': 'value'
        }

        restrictions = {
            'test': [ '$owner' ]
        }

        model = Doc({
            'owner': 'abcd'
        })

        token = {
            'data': {
                'id': 'abcd'
            }
        }

        apply_restrictions(attributes, restrictions, token, model)

        self.assertIsNotNone(attributes.get('test'))

    def test_restrictions_specific_removed(self):

        attributes = {
            'test': 'value'
        }

        restrictions = {
            'test': [ '$owner' ]
        }

        model = Doc({
            'owner': 'abcd'
        })

        token = {
            'data': {
                'id': 'efgh'
            }
        }

        apply_restrictions(attributes, restrictions, token, model)

        self.assertIsNone(attributes.get('test'))

    def test_restrictions_grouped_allowed(self):

        attributes = {
            'test': 'value'
        }

        restrictions = {
            'test': [ '#owner' ]
        }

        model = Doc({
            'owner': ['abcd']
        })

        token = {
            'data': {
                'id': 'abcd'
            }
        }

        apply_restrictions(attributes, restrictions, token, model)

        self.assertIsNotNone(attributes.get('test'))

    def test_restrictions_grouped_removed(self):

        attributes = {
            'test': 'value'
        }

        restrictions = {
            'test': [ '#owner' ]
        }

        model = Doc({
            'owner': ['abcd']
        })

        token = {
            'data': {
                'id': 'abc'
            }
        }

        apply_restrictions(attributes, restrictions, token, model)

        self.assertIsNone(attributes.get('test'))

    def test_restrictions_specific_nested_allowed(self):

        attributes = {
            'test': 'value'
        }

        restrictions = {
            'test': [ '$owner.id' ]
        }

        model = Doc({
            'owner': {
                'id': 'abcd'
            }
        })

        token = {
            'data': {
                'id': 'abcd'
            }
        }

        apply_restrictions(attributes, restrictions, token, model)

        self.assertIsNotNone(attributes.get('test'))

    def test_restrictions_specific_nested_removed(self):

        attributes = {
            'test': 'value'
        }

        restrictions = {
            'test': [ '$owner.id' ]
        }

        model = Doc({
            'owner': {
                'id': 'abcd'
            }
        })

        token = {
            'data': {
                'id': 'abc'
            }
        }

        apply_restrictions(attributes, restrictions, token, model)

        self.assertIsNone(attributes.get('test'))

    def test_restrictions_grouped_nested_allowed(self):

        attributes = {
            'test': 'value'
        }

        restrictions = {
            'test': [ '#owner.id' ]
        }

        model = Doc({
            'owner': {
                'id': ['abcd']
            }
        })

        token = {
            'data': {
                'id': 'abcd'
            }
        }

        apply_restrictions(attributes, restrictions, token, model)

        self.assertIsNotNone(attributes.get('test'))

    def test_restrictions_grouped_nested_removed(self):

        attributes = {
            'test': 'value'
        }

        restrictions = {
            'test': [ '#owner.id' ]
        }

        model = Doc({
            'owner': {
                'id': ['abcd']
            }
        })

        token = {
            'data': {
                'id': 'abc'
            }
        }

        apply_restrictions(attributes, restrictions, token, model)

        self.assertIsNone(attributes.get('test'))
