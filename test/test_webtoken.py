import json
from unittest import skip

from quart import Quart

from sugar_asynctest import AsyncTestCase
from sugar_document import Document
from sugar_odm import MemoryModel, Field

from sugar_api import WebToken


class Authentication(WebToken):

    @classmethod
    async def create(cls, attributes):
        return attributes

    @classmethod
    async def refresh(cls, token):
        return token


class WebTokenTest(AsyncTestCase):

    default_loop = True

    async def asyncSetUp(self):
        self.app = Quart('test')
        self.app.register_blueprint(Authentication.resource())
        self.client = self.app.test_client()
        self.headers = {
            'Accept': 'application/vnd.api+json',
            'Content-Type': 'application/vnd.api+json'
        }

    async def test_create_body_is_not_an_object(self):
        response = await self.client.post('/authentication', headers=self.headers, data=json.dumps([ ]))
        data = await response.get_json()
        self.assertEqual(data['errors'][0]['title'], 'Create Token Error')

    async def test_create_body_empty(self):
        response = await self.client.post('/authentication', headers=self.headers, data=json.dumps({ }))
        data = await response.get_json()
        self.assertEqual(data['errors'][0]['title'], 'Create Token Error')

    async def test_create_data_is_not_an_object(self):
        response = await self.client.post('/authentication', headers=self.headers, data=json.dumps({
            'data': [ ]
        }))
        data = await response.get_json()
        self.assertEqual(data['errors'][0]['title'], 'Create Token Error')

    async def test_create_data_empty(self):
        response = await self.client.post('/authentication', headers=self.headers, data=json.dumps({
            'data': { }
        }))
        data = await response.get_json()
        self.assertEqual(data['errors'][0]['title'], 'Create Token Error')

    async def test_create_attributes_is_not_an_object(self):
        response = await self.client.post('/authentication', headers=self.headers, data=json.dumps({
            'data': {
                'attributes': [ ]
            }
        }))
        data = await response.get_json()
        self.assertEqual(data['errors'][0]['title'], 'Create Token Error')

    async def test_create_attributes_empty(self):
        response = await self.client.post('/authentication', headers=self.headers, data=json.dumps({
            'data': {
                'attributes': { }
            }
        }))
        data = await response.get_json()
        self.assertEqual(data['errors'][0]['title'], 'Create Token Error')

    async def test_create(self):
        response = await self.client.post('/authentication', headers=self.headers, data=json.dumps({
            'data': {
                'attributes': {
                    'field': 'value'
                }
            }
        }))
        data = await response.get_json()
        self.assertIsNotNone(data['data']['token'])

    async def test_refresh_no_token(self):
        response = await self.client.patch('/authentication', headers=self.headers)
        data = await response.get_json()
        self.assertEqual(data['errors'][0]['title'], 'Refresh Token Error')

    async def test_refresh(self):
        response = await self.client.post('/authentication', headers=self.headers, data=json.dumps({
            'data': {
                'attributes': {
                    'field': 'value'
                }
            }
        }))
        data = await response.get_json()
        headers = {
            'Authorization': f'Bearer {data["data"]["token"]}'
        }
        headers.update(self.headers)
        response = await self.client.patch('/authentication', headers=headers)
        data = await response.get_json()
        self.assertIsNotNone(data['data']['token'])
